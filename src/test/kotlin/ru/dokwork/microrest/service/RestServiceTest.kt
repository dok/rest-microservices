package ru.dokwork.microrest.service

import com.nhaarman.mockito_kotlin.*
import com.winterbe.expekt.should
import org.jetbrains.spek.api.Spek
import ru.dokwork.microrest.discovery.Discovery
import java.net.InetSocketAddress
import java.util.*


class RestServiceTest : Spek({
    describe("universal rest-service, that registering himself in discovery service") {
        val discovery = mock<Discovery>()
        doNothing().whenever(discovery).register(any(), any())
        val service = object : RestService("random id", InetSocketAddress(7000), Optional.of(discovery)) {
            var isRouteInvoked = false
            override fun route(http: spark.Service) {
                isRouteInvoked = true
            }
        }

        on("create new instance") {
            it("should register himself in discovery service") {
                verify(discovery).register(argThat { equals(service.id) }, argThat { contains(service.address) })
            }
            it("should invoke route method") {
                service.isRouteInvoked.should.be.`true`
            }
        }
    }
})