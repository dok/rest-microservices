package ru.dokwork.microrest.discovery

import com.nhaarman.mockito_kotlin.*
import org.eclipse.jetty.client.api.ContentResponse
import org.jetbrains.spek.api.Spek
import ru.dokwork.microrest.client.RestServiceClient
import ru.dokwork.microrest.discovery.http.HttpDiscoveryClient
import java.net.InetSocketAddress

class HttpDiscoveryTest : Spek({
    describe("Client to Http discovery service") {
        val serviceClient = mock<RestServiceClient>()
        doReturn(mock<ContentResponse>()).whenever(serviceClient).get(any())
        val discovery = HttpDiscoveryClient(InetSocketAddress(8080), serviceClient)

        on("invoke hosts method") {
            it("should do GET request by path /services/:serviceId") {
                // given:
                val serviceId = "example"
                // when:
                discovery.hosts(serviceId)
                // then:
                verify(serviceClient).get("/services/example")
            }
        }
    }
})