package ru.dokwork.microrest.client

import ru.dokwork.microrest.client.balancer.Balancer
import java.util.*

internal class SimpleBalancer() : Balancer {
    override fun select(nodes: List<Node>): Optional<Node> {
        return Optional.of(nodes.first())
    }
}