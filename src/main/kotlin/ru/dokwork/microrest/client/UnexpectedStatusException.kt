package ru.dokwork.microrest.client

import org.eclipse.jetty.client.api.ContentResponse

// TODO exception message should contains headers and body of the response and inherit more common response exception
open class UnexpectedStatusException(val response: ContentResponse) : Exception("${response.status}")