package ru.dokwork.microrest.client

import org.eclipse.jetty.client.HttpClient
import org.eclipse.jetty.client.api.Request
import org.eclipse.jetty.http.HttpMethod
import java.net.InetSocketAddress
import java.net.URL


/**
 * Representation of concrete service instance
 */
class Node(val address: InetSocketAddress) {

    private val client = HttpClient()
    private var _successes = 0L
    private var _failures = 0L

    /**
     * Count of the successful invocation
     */
    val successes: Long
        get() = synchronized(this) { return _successes }

    /**
     * Count of the failed invocation
     */
    val failures: Long
        get() = synchronized(this) { return _failures }

    init {
        client.start();
    }


    fun createRequest(method: HttpMethod, path: String): Request {
        val url = URL("http", address.hostName, address.port, path)
        return client.newRequest(url.toURI()).method(method)
    }

    fun incrementSuccess() {
        synchronized(this) {
            _successes += 1
            _failures -= 1
        }
    }

    fun incrementFailures() {
        synchronized(this) {
            _successes -= 1
            _failures += 1
        }
    }
}