package ru.dokwork.microrest.client

import org.eclipse.jetty.client.api.ContentProvider
import org.eclipse.jetty.client.api.ContentResponse
import org.eclipse.jetty.client.util.StringContentProvider
import org.eclipse.jetty.http.HttpMethod
import ru.dokwork.microrest.client.balancer.Balancer
import ru.dokwork.microrest.discovery.Discovery
import ru.dokwork.microrest.client.UnexpectedStatusException
import java.net.InetSocketAddress
import java.util.*
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.read
import kotlin.concurrent.write


/**
 * Client to REST-service
 */
open class RestServiceClient(val serviceId: String, val discovery: Discovery, val balancer: Balancer) {
    private val defaultContentType = "application/json"
    private val nodes = ArrayList<Node>()
    private val lock = ReentrantReadWriteLock()

    constructor(serviceId: String, vararg addresses: InetSocketAddress)
    : this(serviceId, SimpleDiscovery(addresses.asList()), SimpleBalancer())

    init {
        takeNodesFromDiscovery()
    }

    open fun get(path: String): ContentResponse {
        return invoke(HttpMethod.GET, path)
    }

    open fun post(path: String, content: String, contentType: String = defaultContentType): ContentResponse {
        return invoke(HttpMethod.POST, path, Optional.of(content))
    }

    open fun put(path: String, content: String, contentType: String = defaultContentType): ContentResponse {
        return invoke(HttpMethod.PUT, path, Optional.of(content))
    }

    open fun delete(path: String): ContentResponse {
        return invoke(HttpMethod.DELETE, path)
    }

    /**
     * Invoke http [method] by [path] with [content].
     * @param method http method
     * @param path url path
     * @param content optional content of the request. It can be omitted
     * @param contentType content type. By default is application/json
     */
    private fun invoke(method: HttpMethod, path: String,
                       content: Optional<String> = Optional.empty(),
                       contentType: String = defaultContentType): ContentResponse {
        val node = selectNode()
        return try {
            var request = node.createRequest(method, path)
            request = content.map { request.content(provider(it), contentType) }.orElse(request)
            val response = approvedResponse(request.send())
            node.incrementSuccess()
            response
        } catch(e: Throwable) {
            node.incrementFailures()
            throw e
        }
    }

    private fun provider(content: String): ContentProvider {
        return StringContentProvider(content)
    }

    /**
     * Перезапрашивает доступные адреса инстансов сервиса у [Discovery]
     */
    private fun takeNodesFromDiscovery() {
        lock.write {
            val hosts = discovery.hosts(serviceId)
            nodes.removeIf { !hosts.contains(it.address) }
            hosts.minus(nodes.map { it.address })
            nodes.addAll(hosts.map { Node(it) })
        }
    }

    /**
     * Делегирует выбор инстанса [Balancer]-ру, если тот не справляется с выбором,
     * перезапрашивает список нод у [Discovery] и переспрашивает балансер.
     */
    private tailrec fun selectNode(): Node {
        lock.read {
            check(nodes.isNotEmpty())

            val node = balancer.select(nodes)
            if (node.isPresent) {
                return node.get()
            } else {
                takeNodesFromDiscovery()
                return selectNode()
            }
        }
    }

    private fun approvedResponse(response: ContentResponse): ContentResponse {
        if (response.status !in 200..299)
            throw UnexpectedStatusException(response)
        else
            return response
    }
}

