package ru.dokwork.microrest.client

import ru.dokwork.microrest.discovery.Discovery
import java.net.InetSocketAddress

internal class SimpleDiscovery(val addresses: List<InetSocketAddress>) : Discovery {

    override fun hosts(serviceId: String): List<InetSocketAddress> {
        return addresses
    }

    override fun register(serviceId: String, addresses: List<InetSocketAddress>) {
        throw UnsupportedOperationException()
    }
}