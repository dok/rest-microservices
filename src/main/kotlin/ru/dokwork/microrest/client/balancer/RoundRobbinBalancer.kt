package ru.dokwork.microrest.client.balancer

import ru.dokwork.microrest.client.Node
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

/**
 * Примитивный балансер выбирающий инстансы поочередно по кругу
 */
class RoundRobbinBalancer : Balancer {

    private val index = AtomicInteger(0)


    override fun select(nodes: List<Node>): Optional<Node> {
        return Optional.of(nodes[index.updateAndGet { x -> cropToSize(x + 1, nodes.size) }])
    }

    private tailrec fun cropToSize(x: Int, size: Int): Int {
        return if (x < size) x else cropToSize(x % size, size)
    }
}