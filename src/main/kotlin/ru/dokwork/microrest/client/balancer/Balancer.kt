package ru.dokwork.microrest.client.balancer

import ru.dokwork.microrest.client.Node
import java.util.*

/**
 * Интерфейс алгоритма балансировки запросов к кластеру сервисов
 */
interface Balancer {

    /**
     * Выбирает наиболее оптимальный инстанс сервиса для дальнейшего с ним взаимодействия
     */
    fun select(nodes: List<Node>): Optional<Node>
}

