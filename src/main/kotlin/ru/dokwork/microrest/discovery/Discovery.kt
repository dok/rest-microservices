package ru.dokwork.microrest.discovery

import java.net.InetSocketAddress
import java.util.concurrent.ConcurrentHashMap

/**
 * Интерфейс сервиса обнаружения других сервисов
 */
interface Discovery {

    fun hosts(serviceId: String): List<InetSocketAddress>

    /**
     * Ассоциирует сервис с укзанным [serviceId] со списком [addresses]
     */
    fun register(serviceId: String, addresses: List<InetSocketAddress>)
}

