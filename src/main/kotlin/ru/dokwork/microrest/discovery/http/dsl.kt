package ru.dokwork.microrest.discovery.http

import java.net.InetSocketAddress

data class JNode(val host: String, val port: Int) {

    fun toInetSocketAddress(): InetSocketAddress {
        return InetSocketAddress(host, port)
    }
}

data class JService(val nodes: Collection<JNode>)

