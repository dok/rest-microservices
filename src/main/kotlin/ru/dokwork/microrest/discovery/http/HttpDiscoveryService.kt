package ru.dokwork.microrest.discovery.http

import org.eclipse.jetty.http.HttpStatus
import ru.dokwork.microrest.service.RestService
import spark.Request
import spark.Response
import spark.Service
import java.net.InetSocketAddress
import java.util.*
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.read
import kotlin.concurrent.write


class HttpDiscoveryService(address: InetSocketAddress) : RestService("http-discovery", address) {
    private val lock = ReentrantReadWriteLock()
    private val services = HashMap<String, MutableSet<JNode>>()

    override fun route(http: Service) {
        http.get("/services/:serviceId", { req, res ->
            res.type("application/json")
            serviceAsJson(req.params(":serviceId"))
        })
        http.put("/services/:serviceId", { req, res ->
            addServiceFromJson(req.params(":serviceId"), req.body())
            res.status(HttpStatus.CREATED_201)
        })
    }

    private fun serviceAsJson(serviceId: String): String {
        lock.read {
            val nodes = services.getOrElse(serviceId, { throw ServiceNotFoundException(serviceId) })
            return Json.serialize(JService(nodes))
        }
    }

    private fun addServiceFromJson(serviceId: String, json: String) {
        val service = Json.deserialize<JService>(json)
        lock.write {
            services.getOrPut(serviceId, { mutableSetOf<JNode>() }).addAll(service.nodes)
        }
    }

    private fun trace(request: Request) {
        if (!log.isTraceEnabled) return

        log.trace(request.toString())
    }

    private fun trace(response: Response): Response {
        if (!log.isTraceEnabled) return response

        log.trace(response.toString())
        return response
    }
}

