package ru.dokwork.microrest.discovery.http

class ServiceNotFoundException(serviceId: String) : Exception("Service with id: [$serviceId] was not registered")