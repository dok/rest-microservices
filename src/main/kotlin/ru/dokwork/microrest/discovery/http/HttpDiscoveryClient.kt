package ru.dokwork.microrest.discovery.http

import ru.dokwork.microrest.client.RestServiceClient
import ru.dokwork.microrest.discovery.Discovery
import java.net.InetSocketAddress


class HttpDiscoveryClient(
        val address: InetSocketAddress,
        private val client: RestServiceClient = RestServiceClient("http-discovery", address)) : Discovery {

    override fun hosts(serviceId: String): List<InetSocketAddress> {
        val content = client.get("/services/$serviceId").contentAsString
        return Json.deserialize<JService>(content).nodes.map { it.toInetSocketAddress() }
    }

    override fun register(serviceId: String, addresses: List<InetSocketAddress>) {
        val nodes = addresses.map { JNode(it.hostString, it.port) }
        val json = Json.serialize(JService(nodes))
        client.put("/services/$serviceId", json)
    }
}