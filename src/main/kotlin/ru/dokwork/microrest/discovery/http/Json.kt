package ru.dokwork.microrest.discovery.http

import com.fasterxml.jackson.module.kotlin.*


object Json {

    val mapper = jacksonObjectMapper()

    inline fun <reified T: Any> deserialize(json: String): T {
        try {
            return mapper.readValue(json)
        } catch(e: Throwable) {
            throw IllegalFormatException(json, e)
        }
    }

    fun serialize(arg: Any): String {
        return mapper.writeValueAsString(arg)
    }

    class IllegalFormatException(json: String, e: Throwable): Exception("JSON:\n>>$json<<", e)
}