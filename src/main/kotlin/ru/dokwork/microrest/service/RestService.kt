package ru.dokwork.microrest.service

import org.slf4j.LoggerFactory
import ru.dokwork.microrest.discovery.Discovery
import spark.Service
import spark.Spark
import java.net.InetSocketAddress
import java.util.*

/**
 * Universal representation of the REST service, that register itself in discovery services.
 * @param id unique service identifier
 * @param address inet address of the service
 * @param discovery client to discovery service for registration of the service
 */
abstract class RestService(
        val id: String,
        val address: InetSocketAddress,
        val discovery: Optional<Discovery> = Optional.empty()) : Runnable {

    protected val log = LoggerFactory.getLogger(javaClass)

    override fun run() {
        val http = Service.ignite()
        http.ipAddress(address.hostString)
        http.port(address.port)
        discovery.map { it.register(id, listOf(address)) }
        http.exception(Exception::class.java, { e, request, response ->
            log.error("Exception in service $id", e)
            response.body(e.stackTrace.joinToString("\n"))
        })
        route(http)
    }

    abstract fun route(http: Service)
}