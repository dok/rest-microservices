package ru.dokwork.kommon

import java.util.function.Consumer
import java.util.function.Function
import java.util.function.Predicate
import java.util.function.Supplier

interface Monad<T> {

    fun get(): T

    fun isPresent(): Boolean

    fun ifPresent(consumer: Consumer<in T>)

    fun filter(predicate: Predicate<in T>): Monad<T>

    fun <U> map(mapper: Function<in T, out U>): Monad<U>

//    fun <U> flatMap(mapper: Function<in T, in Monad<U>>): Monad<U>

    fun orElse(other: T): T

    fun orElseGet(other: Supplier<out T>): T

    fun <X : Throwable> orElseThrow(exceptionSupplier: Supplier<out X>): T
}
