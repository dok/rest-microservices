package ru.dokwork.kommon

import java.util.*
import java.util.function.Consumer
import java.util.function.Function
import java.util.function.Predicate
import java.util.function.Supplier

class Option<T> internal constructor(val opt: Optional<T>) : Monad<T> {
    companion object OptionBuilder {
        fun <T1> empty(): Option<T1> {
            return Option(Optional.empty<T1>())
        }

        fun <T1> of(value: T1): Option<T1> {
            return Option(Optional.of(value))
        }

        fun <T1> ofNullable(value: T1): Option<T1> {
            return Option(Optional.ofNullable(value))
        }
    }

    override fun get(): T {
        return opt.get()
    }

    override fun isPresent(): Boolean {
        return opt.isPresent()
    }

    override fun ifPresent(consumer: Consumer<in T>) {
        opt.ifPresent(consumer)
    }

    override fun filter(predicate: Predicate<in T>): Option<T> {
        return Option(opt.filter(predicate))
    }

    override fun <U> map(mapper: Function<in T, out U>): Option<U> {
        return Option(opt.map(mapper))
    }

    fun <U> flatMap(mapper: Function<in T, Option<U>>): Option<U> {
        val m = Function<T, java.util.Optional<U>> { t -> mapper.apply(t).opt }
        return Option(opt.flatMap(m))
    }

    override fun orElse(other: T): T {
        return opt.orElse(other)
    }

    override fun orElseGet(other: Supplier<out T>): T {
        return opt.orElseGet(other)
    }

    override fun <X : Throwable> orElseThrow(exceptionSupplier: Supplier<out X>): T {
        return opt.orElseThrow(exceptionSupplier)
    }

    override fun equals(obj: Any?): Boolean {
        return opt == obj
    }

    override fun hashCode(): Int {
        return opt.hashCode()
    }

    override fun toString(): String {
        return opt.toString()
    }

    fun asOptional() = Optional.ofNullable(opt.get())
}
