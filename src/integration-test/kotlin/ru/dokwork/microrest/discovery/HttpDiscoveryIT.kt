package ru.dokwork.microrest.discovery

import com.winterbe.expekt.should
import org.jetbrains.spek.api.Spek
import ru.dokwork.microrest.client.RestServiceClient
import ru.dokwork.microrest.discovery.http.HttpDiscoveryClient
import ru.dokwork.microrest.discovery.http.HttpDiscoveryService
import java.net.InetSocketAddress

class HttpDiscoveryIT : Spek({

    val address = InetSocketAddress(9089)
    val service = HttpDiscoveryService(address)
    val client = HttpDiscoveryClient(address)

    given("Run discovery service") {
        service.run()
    }

    describe("test interaction with service by client") {
        val serviceId = "ServiceForTestWithClient"
        val nodes = arrayOf(InetSocketAddress(1001), InetSocketAddress(1002))

        on("registered new service with id: [$serviceId]") {
            client.register(serviceId, nodes.asList())
        }
        on("check host for service with id: [$serviceId]") {
            client.hosts(serviceId).should.contain.elements(*nodes)
        }
    }

    describe("interaction with discovery by http") {
        val http = RestServiceClient("http-discovery", address)

        on("invoke PUT on /services/:serviceId") {
            // given:
            val serviceId = "ServiceForTesByHttp"
            val json = """{"nodes":[{"host":"localhost", "port":6001}, {"host":"localhost", "port":6002}]}"""
            // when:
            val response = http.put("/services/$serviceId", json)
            // then:
            response.status.should.be.equal(201)
            client.hosts(serviceId).should.contain.elements(
                    InetSocketAddress("localhost", 6001), InetSocketAddress("localhost", 6002)
            )
        }
    }
})