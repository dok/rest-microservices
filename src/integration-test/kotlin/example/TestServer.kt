package example

import ru.dokwork.microrest.client.RestServiceClient
import spark.Spark.exception
import spark.Spark.get
import spark.Spark.port
import java.net.InetSocketAddress

fun main(args: Array<String>) {

    port(7000)

    val service = RestServiceClient(
            serviceId = "finagle",
            addresses = *arrayOf(InetSocketAddress(7001), InetSocketAddress(7002))
    )

    get("/", { request, response ->
        service.get("/").contentAsString
    })

    exception(Exception::class.java, { e, request, response ->
        println(e.toString() + "\n" + e.stackTrace.joinToString("\n"))
    })

    println("Server started on port 7000")
}


