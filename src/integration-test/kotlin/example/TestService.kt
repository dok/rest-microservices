package example

import spark.Spark.*

/**
 * Примитивный сервис возвращающий номер порта на котором он запущен.
 * Порт задается первым аргументом запуска.
 */
fun main(args: Array<String>) {

    val port = if (args.isNotEmpty()) args[0].toInt() else 7000

    port(port)

    get("/", { req, res -> args[0] + "\n"})

    println("Service started on port $port")
}